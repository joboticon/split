// AboutApp.qml
//
// This file is part of the Say Ubuntu application.
//
// Copyright (c) 2017 
//
// Maintained by Joe (@exar_kun) <joe@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import Ubuntu.Components 1.3
import "modules"

Page {
    id: aboutPage
    title: i18n.tr("Split")
    header: DefaultHeader {}

    ScrollView {
        id: scroll
        anchors {
            fill: parent
            topMargin: aboutPage.header.height
        }


        Column {
            id: workoutColumn
            width: scroll.width
            spacing: units.gu(3)

            DefaultLabel {
                text: i18n.tr("Workouts")
                textSize: Label.XLarge
            }

            DefaultLabel {
                text: i18n.tr("Workout 1: Relaxed Splits")
                textSize: Label.Large
            }

            DefaultLabel {
                horizontalAlignment: Text.AlignHLeft
                text: " • As many warm up sets of relaxed splits to get to your maximum depth.\n" +
                      " • 3 sets of relaxed splits for any single split variation held for 3 minutes.\n" + 
                      " • Rest more than 2 minutes between sets.\n" +
                      " • Walk around and move lightly, don’t sit or stretch.\n"

            }

            DefaultLabel {
                text: i18n.tr("Workout 2: Weighted Splits")
                textSize: Label.Large
            }

            DefaultLabel {
                horizontalAlignment: Text.AlignHLeft
                text: " • As many warm up sets of relaxed splits to get to your maximum depth.\n" +
                      " • Rest 2 minutes between your warm up sets. No more! No less!\n" +
                      " • 3-5 sets of weighted splits for any one split variation.\n" +
                      " • 3-10 seconds held at maximum depth for side splits.\n" +
                      " • 6-20 seconds held at maximum depth for front splits.\n" +
                      " • Rest 3:30-5:30 between sets. No less!\n" +
                      " • Walk around and move lightly, don’t sit down or stretch.\n"

            }
        }
    }
}
