// Main.qml
//
// This file is part of the Split application.
//
// Copyright (c) 2017 
//
// Maintained by Joe (@exar_kun) <joe@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import QtQuick.Window 2.2
import Qt.labs.settings 1.0
import "modules"

Window {
    id: split
    title: i18n.tr("Split")
    width: units.gu(150)
    height: units.gu(100)
    minimumWidth: units.gu(45)
    minimumHeight: units.gu(45)
    maximumWidth: Screen.width
    maximumHeight: Screen.height

    property string version: "1.2a"

    Component.onCompleted: {
        console.log("Split started\n")
        console.log("Version: " + version);
    }

    MainView {
        id: root
        objectName: 'mainView'
        applicationName: 'split.joe'
        automaticOrientation: true
        anchorToKeyboard: true
        anchors.fill: parent

 Settings {
        id: settings
        property bool setWeek11
        property bool setWeek12
        property bool setWeek13
        property bool setWeek14
        property bool setWeek21
        property bool setWeek22
        property bool setWeek23
        property bool setWeek24
        property bool setWeek25
        property bool setWeek31
        property bool setWeek32
        property bool setWeek33
        property bool setWeek34
        property bool setWeek41
        property bool setWeek42
        property bool setWeek43
        property bool setWeek50
        property bool setWeek61
        property bool setWeek62
        property bool setWeek71
        property bool setWeek72
        property bool setWeek73
        property bool setWeek74
        property bool setWeek81
        property bool setWeek82
        property bool setWeek83
        property bool setWeek84
        property bool setWeek91
        property bool setWeek92
        property bool setWeek93
        property bool setWeek100
        property bool setWeek111
        property bool setWeek112
        property bool setWeek120
    }
        PageStack {
            id: mainStack
        }

        Component.onCompleted: {
            mainStack.clear()
            mainStack.push(Qt.resolvedUrl("Home.qml"))
        }
    }
}
