// Home.qml
//
// This file is part of the Split application.
//
// Copyright (c) 2017 
//
// Maintained by Joe (@exar_kun) <joe@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.6
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import "modules"
import Ubuntu.Components.Popups 1.3
import UserMetrics 0.1

Page {
    id: homePage
    title: i18n.tr("Split")
    header: DefaultHeader {}

    ScrollView {
        id: scroll
        anchors {
            fill: parent
            topMargin: homePage.header.height
        }

        Column {
            id: homeColumn
            width: scroll.width
            spacing: units.gu(3)

	 DefaultLabel {
	      id: programName
              text: i18n.tr("Legendary Flexibilty")
              }

            Metric {

                id: metric1
                name: "heythere"
                format: "Hey there"
                emptyFormat: "Hey there"
                domain: "split.joe"
            }

            Metric {

                id: metric2
                name: "hithere"
                format: "Hi there"
                emptyFormat: "Hi there"
                domain: "split.joe"
            }

         RowLabel {
                    id: week1
                    text: i18n.tr('Week 1')
                }

                DefaultRow {
                    spacing: units.gu(5)
                    
                    CheckBox {
                        id: check11
                        text: i18n.tr('W1')
                        checked: settings.setWeek11
                        onClicked: (settings.setWeek11 == true) ? settings.setWeek11 = false : settings.setWeek11 = true
                    }

                    CheckBox {
                        id: check12
                        text: i18n.tr('W1')
                        checked: settings.setWeek12
                        onClicked: (settings.setWeek12 == true) ? settings.setWeek12 = false : settings.setWeek12 = true
                    }
                    CheckBox {
                        id: check13
                        text: i18n.tr('W2')
                        checked: settings.setWeek13
                        onClicked: (settings.setWeek13 == true) ? settings.setWeek13 = false : settings.setWeek13 = true
                    }

                    CheckBox {
                        id: check14
                        text: i18n.tr('W2')
                        checked: settings.setWeek14
                        onClicked: (settings.setWeek14 == true) ? settings.setWeek14 = false : settings.setWeek14 = true
                    }
                }

                RowLabel {
                    id: week2
                    text: i18n.tr('Week 2')
                }

                DefaultRow {
                    spacing: units.gu(5)

                    CheckBox {
                        id: check21
                        text: i18n.tr('W1')
                        checked: settings.setWeek21
                        onClicked: (settings.setWeek21 == true) ? settings.setWeek21 = false : settings.setWeek21 = true
                    }

                    CheckBox {
                        id: check22
                        text: i18n.tr('W1')
                        checked: settings.setWeek22
                        onClicked: (settings.setWeek22 == true) ? settings.setWeek22 = false : settings.setWeek22 = true
                    }

                    CheckBox {
                        id: check23
                        text: i18n.tr('W1')
                        checked: settings.setWeek23
                        onClicked: (settings.setWeek23 == true) ? settings.setWeek23 = false : settings.setWeek23 = true
                    }

                    CheckBox {
                        id: check24
                        text: i18n.tr('W1')
                        checked: settings.setWeek24
                        onClicked: (settings.setWeek24 == true) ? settings.setWeek24 = false : settings.setWeek24 = true
                    }
                    CheckBox {
                        id: check25
                        text: i18n.tr('W2')
                        checked: settings.setWeek25
                        onClicked: (settings.setWeek25 == true) ? settings.setWeek25 = false : settings.setWeek25 = true
                    }
                }

                RowLabel {
                    id: week3
                    text: i18n.tr('Week 3')
                }

                DefaultRow {
                    spacing: units.gu(5)

                    CheckBox {
                        id: check31
                        text: i18n.tr('W1')
                        checked: settings.setWeek31
                        onClicked: (settings.setWeek31 == true) ? settings.setWeek31 = false : settings.setWeek31 = true
                    }

                    CheckBox {
                        id: check32
                        text: i18n.tr('W1')
                        checked: settings.setWeek32
                        onClicked: (settings.setWeek32 == true) ? settings.setWeek32 = false : settings.setWeek32 = true
                    }
                    CheckBox {
                        id: check33
                        text: i18n.tr('W2')
                        checked: settings.setWeek33
                        onClicked: (settings.setWeek33 == true) ? settings.setWeek33 = false : settings.setWeek33 = true
                    }

                    CheckBox {
                        id: check34
                        text: i18n.tr('W2')
                        checked: settings.setWeek34
                        onClicked: (settings.setWeek34 == true) ? settings.setWeek34 = false : settings.setWeek34 = true
                    }
                }

                RowLabel {
                    id: week4
                    text: i18n.tr('Week 4')
                }

                DefaultRow {
                    spacing: units.gu(5)

                    CheckBox {
                        id: check41
                        text: i18n.tr('W2')
                        checked: settings.setWeek41
                        onClicked: (settings.setWeek41 == true) ? settings.setWeek41 = false : settings.setWeek41 = true
                    }

                    CheckBox {
                        id: check42
                        text: i18n.tr('W2')
                        checked: settings.setWeek42
                        onClicked: (settings.setWeek42 == true) ? settings.setWeek42 = false : settings.setWeek42 = true
                    }
                    CheckBox {
                        id: check43
                        text: i18n.tr('W2')
                        checked: settings.setWeek43
                        onClicked: (settings.setWeek43 == true) ? settings.setWeek43 = false : settings.setWeek43 = true
                    }
                }

                RowLabel {
                    id: week5
                    text: i18n.tr('Week 5')
                }

                DefaultRow {
                    spacing: units.gu(5)

                    CheckBox {
                        id: check50
                        text: i18n.tr('REST')
                        checked: settings.setWeek50
                        onClicked: (settings.setWeek50 == true) ? settings.setWeek50 = false : settings.setWeek50 = true
                    }
                }

                RowLabel {
                    id: week6
                    text: i18n.tr('Week 6')
                }

                DefaultRow {
                    spacing: units.gu(5)

                    CheckBox {
                        id: check61
                        text: i18n.tr('W1')
                        checked: settings.setWeek61
                        onClicked: (settings.setWeek61 == true) ? settings.setWeek61 = false : settings.setWeek61 = true
                    }

                    CheckBox {
                        id: check62
                        text: i18n.tr('W1')
                        checked: settings.setWeek62
                        onClicked: (settings.setWeek62 == true) ? settings.setWeek62 = false : settings.setWeek62 = true
                    }
                }

                RowLabel {
                    id: week7
                    text: i18n.tr('Week 7')
                }

                DefaultRow {
                    spacing: units.gu(5)

                    CheckBox {
                        id: check71
                        text: i18n.tr('W1')
                        checked: settings.setWeek71
                        onClicked: (settings.setWeek71 == true) ? settings.setWeek71 = false : settings.setWeek71 = true
                    }

                    CheckBox {
                        id: check72
                        text: i18n.tr('W1')
                        checked: settings.setWeek72
                        onClicked: (settings.setWeek72 == true) ? settings.setWeek72 = false : settings.setWeek72 = true
                    }
                    CheckBox {
                        id: check73
                        text: i18n.tr('W2')
                        checked: settings.setWeek73
                        onClicked: (settings.setWeek73 == true) ? settings.setWeek73 = false : settings.setWeek73 = true
                    }

                    CheckBox {
                        id: check74
                        text: i18n.tr('W2')
                        checked: settings.setWeek74
                        onClicked: (settings.setWeek74 == true) ? settings.setWeek74 = false : settings.setWeek74 = true
                    }
                }


                RowLabel {
                    id: week8
                    text: i18n.tr('Week 8')
                }

                DefaultRow {
                    spacing: units.gu(5)

                    CheckBox {
                        id: check81
                        text: i18n.tr('W1')
                        checked: settings.setWeek81
                        onClicked: (settings.setWeek81 == true) ? settings.setWeek81 = false : settings.setWeek81 = true
                    }

                    CheckBox {
                        id: check82
                        text: i18n.tr('W1')
                        checked: settings.setWeek82
                        onClicked: (settings.setWeek82 == true) ? settings.setWeek82 = false : settings.setWeek82 = true
                    }
                    CheckBox {
                        id: check83
                        text: i18n.tr('W1')
                        checked: settings.setWeek83
                        onClicked: (settings.setWeek83 == true) ? settings.setWeek83 = false : settings.setWeek83 = true
                    }

                    CheckBox {
                        id: check84
                        text: i18n.tr('W2')
                        checked: settings.setWeek84
                        onClicked: (settings.setWeek84 == true) ? settings.setWeek84 = false : settings.setWeek84 = true
                    }
                }

                RowLabel {
                    id: week9
                    text: i18n.tr('Week 9')
                }

                DefaultRow {
                    spacing: units.gu(5)

                    CheckBox {
                        id: check91
                        text: i18n.tr('W2')
                        checked: settings.setWeek91
                        onClicked: (settings.setWeek91 == true) ? settings.setWeek91 = false : settings.setWeek91 = true
                    }

                    CheckBox {
                        id: check92
                        text: i18n.tr('W2')
                        checked: settings.setWeek92
                        onClicked: (settings.setWeek92 == true) ? settings.setWeek92 = false : settings.setWeek92 = true
                    }
                    CheckBox {
                        id: check93
                        text: i18n.tr('W2')
                        checked: settings.setWeek93
                        onClicked: (settings.setWeek93 == true) ? settings.setWeek93 = false : settings.setWeek93 = true
                    }
                }

                RowLabel {
                    id: week10
                    text: i18n.tr('Week 10')
                }

                DefaultRow {
                    spacing: units.gu(5)

                    CheckBox {
                        id: check100
                        text: i18n.tr('REST')
                        checked: settings.setWeek100
                        onClicked: (settings.setWeek100 == true) ? settings.setWeek100 = false : settings.setWeek100 = true
                    }
                }

                RowLabel {
                    id: week11
                    text: i18n.tr('Week 11')
                }

                DefaultRow {
                    spacing: units.gu(5)

                    CheckBox {
                        id: check111
                        text: i18n.tr('W1')
                        checked: settings.setWeek111
                        onClicked: (settings.setWeek111 == true) ? settings.setWeek111 = false : settings.setWeek111 = true
                    }
                    CheckBox {
                        id: check112
                        text: i18n.tr('W1')
                        checked: settings.setWeek112
                        onClicked: (settings.setWeek112 == true) ? settings.setWeek112 = false : settings.setWeek112 = true
                    }
                }

                RowLabel {
                    id: week12
                    text: i18n.tr('Week 12')
                }

                DefaultRow {
                    spacing: units.gu(5)

                    CheckBox {
                        id: check121
                        text: i18n.tr('TEST MAX')
                        checked: settings.setWeek120
                        onClicked: (settings.setWeek120 == true) ? settings.setWeek120 = false : settings.setWeek120 = true
                    }
                }



                
                DefaultRow {
                    Button {
                        id: resetButton
                        text: i18n.tr('Reset')
                        onClicked: PopupUtils.open(Qt.resolvedUrl("ResetDialog.qml"))
                    }
                }

                RowLabel{
                    id: lastRow
                    text: " "
                }
          
        }
    }
}
