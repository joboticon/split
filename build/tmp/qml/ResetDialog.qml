
import QtQuick 2.6
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import "modules"
import Ubuntu.Components.Popups 1.3

Dialog {
    id: reset
    text: i18n.tr("Reset workouts?")    

    signal dialogCanceled
    signal dialogAccepted



    Button {
        id: okButton
        
        text: i18n.tr("Reset")
        color: UbuntuColors.green
        onClicked:  {
            dialogAccepted();
            settings.setWeek11 = false;
            settings.setWeek12 = false;
            settings.setWeek13 = false;
            settings.setWeek14 = false;
            settings.setWeek21 = false;
            settings.setWeek22 = false;
            settings.setWeek23 = false;
            settings.setWeek24 = false;
            settings.setWeek25 = false;
            settings.setWeek31 = false;
            settings.setWeek32 = false;
            settings.setWeek33 = false;
            settings.setWeek34 = false;
            settings.setWeek41 = false;
            settings.setWeek42 = false;
            settings.setWeek43 = false;
            settings.setWeek50 = false;
            settings.setWeek61 = false;
            settings.setWeek62 = false;
            settings.setWeek71 = false;
            settings.setWeek72 = false;
            settings.setWeek73 = false;
            settings.setWeek74 = false;
            settings.setWeek81 = false;
            settings.setWeek82 = false;
            settings.setWeek83 = false;
            settings.setWeek84 = false;
            settings.setWeek91 = false;
            settings.setWeek92 = false;
            settings.setWeek93 = false;
            settings.setWeek100 = false;
            settings.setWeek111 = false;
            settings.setWeek112 = false;
            settings.setWeek120 = false;
            PopupUtils.close(reset);
        }
    }

    Button {
        id: cancelButton
        text: i18n.tr("Cancel")
        color: UbuntuColors.red
        onClicked: {
            dialogCanceled();
            PopupUtils.close(reset);
        }
    }
}
