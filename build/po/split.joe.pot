# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the split.joe package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: split.joe\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-23 19:26+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/ResetDialog.qml:10
msgid "Reset workouts?"
msgstr ""

#: ../qml/ResetDialog.qml:20 ../qml/Home.qml:398
msgid "Reset"
msgstr ""

#: ../qml/ResetDialog.qml:64
msgid "Cancel"
msgstr ""

#: ../qml/AboutApp.qml:31 ../qml/Workouts.qml:31 ../qml/Main.qml:34
#: ../qml/Home.qml:33 split.joe.desktop.in.h:1
msgid "Split"
msgstr ""

#: ../qml/AboutApp.qml:47
msgid "About Split"
msgstr ""

#: ../qml/AboutApp.qml:61
msgid "Version: "
msgstr ""

#: ../qml/AboutApp.qml:65
msgid "Achieve Legendary Flexibility"
msgstr ""

#. TRANSLATORS: Description of the menu item
#: ../qml/Workouts.qml:48 ../qml/modules/DefaultHeader.qml:37
msgid "Workouts"
msgstr ""

#: ../qml/Workouts.qml:53
msgid "Workout 1: Relaxed Splits"
msgstr ""

#: ../qml/Workouts.qml:67
msgid "Workout 2: Weighted Splits"
msgstr ""

#: ../qml/Home.qml:50
msgid "Legendary Flexibilty"
msgstr ""

#: ../qml/Home.qml:55
msgid "Week 1"
msgstr ""

#: ../qml/Home.qml:63 ../qml/Home.qml:70 ../qml/Home.qml:99 ../qml/Home.qml:106
#: ../qml/Home.qml:113 ../qml/Home.qml:120 ../qml/Home.qml:142
#: ../qml/Home.qml:149 ../qml/Home.qml:223 ../qml/Home.qml:230
#: ../qml/Home.qml:246 ../qml/Home.qml:253 ../qml/Home.qml:283
#: ../qml/Home.qml:290 ../qml/Home.qml:296 ../qml/Home.qml:364
#: ../qml/Home.qml:370
msgid "W1"
msgstr ""

#: ../qml/Home.qml:76 ../qml/Home.qml:83 ../qml/Home.qml:126
#: ../qml/Home.qml:155 ../qml/Home.qml:162 ../qml/Home.qml:178
#: ../qml/Home.qml:185 ../qml/Home.qml:191 ../qml/Home.qml:259
#: ../qml/Home.qml:266 ../qml/Home.qml:303 ../qml/Home.qml:319
#: ../qml/Home.qml:326 ../qml/Home.qml:332
msgid "W2"
msgstr ""

#: ../qml/Home.qml:91
msgid "Week 2"
msgstr ""

#: ../qml/Home.qml:134
msgid "Week 3"
msgstr ""

#: ../qml/Home.qml:170
msgid "Week 4"
msgstr ""

#: ../qml/Home.qml:199
msgid "Week 5"
msgstr ""

#: ../qml/Home.qml:207 ../qml/Home.qml:348
msgid "REST"
msgstr ""

#: ../qml/Home.qml:215
msgid "Week 6"
msgstr ""

#: ../qml/Home.qml:238
msgid "Week 7"
msgstr ""

#: ../qml/Home.qml:275
msgid "Week 8"
msgstr ""

#: ../qml/Home.qml:311
msgid "Week 9"
msgstr ""

#: ../qml/Home.qml:340
msgid "Week 10"
msgstr ""

#: ../qml/Home.qml:356
msgid "Week 11"
msgstr ""

#: ../qml/Home.qml:378
msgid "Week 12"
msgstr ""

#: ../qml/Home.qml:386
msgid "TEST MAX"
msgstr ""

#. TRANSLATORS: Description of the menu item
#: ../qml/modules/DefaultHeader.qml:43
msgid "About This App"
msgstr ""
